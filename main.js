// Game state
let currentPlayer = "X";
let gameBoard = [
  ["", "", ""],
  ["", "", ""],
  ["", "", ""],
];

// DOM elements
const cells = document.querySelectorAll(".cell");
const restartBtn = document.getElementById("restart-btn");

// Add event listeners
cells.forEach((cell) => {
  cell.addEventListener("click", handleCellClick);
});

restartBtn.addEventListener("click", restartGame);

// Handle cell click
function handleCellClick(event) {
  const cell = event.target;
  const rowIndex = cell.parentNode.rowIndex;
  const cellIndex = cell.cellIndex;

  // Check if the cell is already occupied
  if (gameBoard[rowIndex][cellIndex] !== "") {
    return;
  }

  // Update game state
  gameBoard[rowIndex][cellIndex] = currentPlayer;

  // Update cell content
  cell.textContent = currentPlayer;

  // Check for a win or a draw
  if (checkWin() || checkDraw()) {
    endGame();
  } else {
    // Switch players
    currentPlayer = currentPlayer === "X" ? "O" : "X";
  }
}

// Check for a win
function checkWin() {
  // Check rows
  for (let i = 0; i < 3; i++) {
    if (
      gameBoard[i][0] === currentPlayer &&
      gameBoard[i][1] === currentPlayer &&
      gameBoard[i][2] === currentPlayer
    ) {
      return true;
    }
  }

  // Check columns
  for (let i = 0; i < 3; i++) {
    if (
      gameBoard[0][i] === currentPlayer &&
      gameBoard[1][i] === currentPlayer &&
      gameBoard[2][i] === currentPlayer
    ) {
      return true;
    }
  }

  // Check diagonals
  if (
    (gameBoard[0][0] === currentPlayer &&
      gameBoard[1][1] === currentPlayer &&
      gameBoard[2][2] === currentPlayer) ||
    (gameBoard[0][2] === currentPlayer &&
      gameBoard[1][1] === currentPlayer &&
      gameBoard[2][0] === currentPlayer)
  ) {
    return true;
  }

  return false;
}

// Check for a draw
function checkDraw() {
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      if (gameBoard[i][j] === "") {
        return false;
      }
    }
  }
  return true;
}

// End the game
function endGame() {
  cells.forEach((cell) => {
    cell.removeEventListener("click", handleCellClick);
  });
}

// Restart the game
function restartGame() {
  currentPlayer = "X";
  gameBoard = [
    ["", "", ""],
    ["", "", ""],
    ["", "", ""],
  ];

  cells.forEach((cell) => {
    cell.textContent = "";
    cell.addEventListener("click", handleCellClick);
  });
}
